package com.example.cotizacion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class CotizacionActivity extends AppCompatActivity {
private Cotizacion cotizacion;
private TextView lblFolio;
private TextView lblNombre;
private Button btnCal;
private Button btnLimpiar;
private Button btnCerrar;
private EditText txtDescripcion;
private EditText txtValor;
private EditText txtPorcentaje;
private RadioGroup rGroup;
private RadioButton r12;
private RadioButton r18;
private RadioButton r24;
private RadioButton r36;
private TextView lblPago;
private TextView lblEnganche;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cotizacion);
        lblFolio=(TextView)findViewById(R.id.lblFolio);
        lblNombre=(TextView)findViewById(R.id.lblNombre);
        btnCal=(Button) findViewById(R.id.btnCal);
        btnLimpiar=(Button) findViewById(R.id.btnLimpiar);
        btnCerrar=(Button)findViewById(R.id.btnCerrar);
        txtDescripcion=(EditText)findViewById(R.id.txtDescripcion);
        txtValor=(EditText)findViewById(R.id.txtValor);
        txtPorcentaje=(EditText)findViewById(R.id.txtPorcentaje);
        rGroup=(RadioGroup)findViewById(R.id.rGroup);
        r12=(RadioButton)findViewById(R.id.r12);
        r18=(RadioButton)findViewById(R.id.r18);
        r24=(RadioButton)findViewById(R.id.r24);
        r36=(RadioButton)findViewById(R.id.r36);
        lblPago=(TextView)findViewById(R.id.lblPago);
        lblEnganche=(TextView)findViewById(R.id.lblEnganche);

        Bundle datos = getIntent().getExtras();

        lblNombre.setText("Nombre de cliente: "+datos.getString("cliente"));

        cotizacion = (Cotizacion) datos.getSerializable("cotizacion");

        lblFolio.setText("Folio: " + String.valueOf(cotizacion.getFolio()));

        btnCal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Descrip = txtDescripcion.getText().toString();
                String Val = txtValor.getText().toString();
                String Por = txtPorcentaje.getText().toString();
                if (Descrip.matches("") || Val.matches("") || Por.matches("")) {
                    Toast.makeText(CotizacionActivity.this, "Falto capturar algun dato", Toast.LENGTH_LONG).show();
                } else {

                    cotizacion.setValorAuto(Float.valueOf(txtValor.getText().toString()));
                    cotizacion.setPorEnganche(Float.valueOf(txtPorcentaje.getText().toString()));
                    int plazo = 0;
                    if (r12.isChecked()) {
                        plazo = 12;
                    } else if (r18.isChecked()) {
                        plazo = 18;
                    } else if (r24.isChecked()) {
                        plazo = 24;
                    } else if (r36.isChecked()) {
                        plazo = 36;
                    }
                    cotizacion.setPlazos(plazo);


                    lblPago.setText("Pago mensual: " + String.valueOf(cotizacion.calcularPagoMensual()));
                    lblEnganche.setText("Enganche: " + String.valueOf(cotizacion.calcularEnganche()));
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            txtDescripcion.setText("");
            txtValor.setText("");
            txtPorcentaje.setText("");
            lblPago.setText("Pago mensual: ");
            lblEnganche.setText("Enganche: ");
            r12.setChecked(true);
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
